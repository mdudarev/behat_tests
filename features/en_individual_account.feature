# language: en

Feature: Grow Practice - Account
  Testing account creation and signature.
 
  @javascript
  Scenario: Авторизуемся, создаём проспект, проверяем статус, создаём аккаунт и отправляем документы на подписание, проверяем статусы и удаляем проспект.
    Given I am on homepage
    And I am not authorized
    And I log in as a user "Mikhail" with a password "Pa$$word_1"
    Then I should be authorized
    Then I should see "Welcome to Vanare"
    And I follow "Grow Practice"
    And I follow "+ Add New Prospect"
    Then I create prospect with fields:
    |First Name|Last Name|Advisor Code|Work Email|
    |Mikahil|Dudarev|TEST|md+1@4xxi.com|
    Then I click on "//span[@class='icon action']"
    And I click on "//div[contains(@id,'edit-tab')]//a[contains(text(),'Operational Status')]"
    #Then I should see элемент "//div[@id='modal-first-step' and @class='status-block-step -active']"
    #And I should see элемент "//div[@id='modal-sixth-step' and @class='status-block-step -active']"
    Then I click on "//div[@class='modal-dialog']//button[@class='close']"
    And I click on "//span[text()='+ Account'][ancestor::tr/td[text()='Dudarev']]"
    And I click on "//label/p[text()='Individual']"
    And I press on "button" by js
    Then I fill in "777" for "vanare_websitebundle_newaccount_workflow_step2_phone1"
    Then I fill in "777" for "vanare_websitebundle_newaccount_workflow_step2_phone2"
    Then I fill in "7777" for "vanare_websitebundle_newaccount_workflow_step2_phone3"
    Then I fill in "Test Street" for "vanare_websitebundle_newaccount_workflow_step2_homeAddress1"
    Then I fill in "Orenburg" for "vanare_websitebundle_newaccount_workflow_step2_homeCity"
    Then I fill in "46545" for "vanare_websitebundle_newaccount_workflow_step2_homeZip"
    
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_USCitizenship_0']"
    And I press on "label[for=vanare_websitebundle_newaccount_workflow_step2_ssn_1]" by js
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_ssn_1']"
   

    Then I enter "11" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_month']"

    Then I enter "12" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_day']"

    Then I enter "1988" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_year']"
    
    

    And I select "Single" from "vanare_websitebundle_newaccount_workflow_step2_maritialStatus"
    Then I enter "2" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dependents']"
    
    Then I fill in "Test this" for "vanare_websitebundle_newaccount_workflow_step2_driverLicense"
    Then I fill in "03/27/2015" for "vanare_websitebundle_newaccount_workflow_step2_expiration"

    
    And I focus on element "//select[@id='vanare_websitebundle_newaccount_workflow_step2_employment']"
    And I select "Unemployed" from "vanare_websitebundle_newaccount_workflow_step2_employment"
    And I focus on element "//input[@id='vanare_websitebundle_newaccount_workflow_step2_sourceOfIncome']"
    Then I fill in "Test this" for "vanare_websitebundle_newaccount_workflow_step2_sourceOfIncome"   
    
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsFinancial_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsFinancial_1']"
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsDirector_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsDirector_1']"
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsPolitical_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsPolitical_1']"
    
    And I press on "button" by js
    And я не вижу ошибки

    And I should see "Beneficiary Information"
    And I press on "button" by js
    
    And I should see "Cash Sweep Vehicle"
    And I press on "button" by js

    And I should see "Proxy Authorization"
    And I press on "button" by js

    And I should see "Duplicate Accounts"
    And I press on "button" by js

    And I should see "Margin"
    And I press on "button" by js

    And I should see "Personal Bank Account Information"
    And I press on "button" by js

    And I should see "Transfer Assets into Your Account"
    And I press on "button" by js
    
    And I should see "E-Signature"
    And I press "vanare_websitebundle_newaccount_workflow_step14_submit"

    And I wait element "p:contains('New Account Documents sent to Mikahil Dudarev')"


    Then follow "Grow Practice"

    #!!! SIGNATURE

    Then я создаю новую сессию "mail"
    Then go to "http://gmail.com"
    And I fill in "md@4xxi.com" for "Email"
    And I fill in "$58" for "Passwd"
    Then I press "signIn"
    And I wait element "span:contains('Почта')"
    And I click on "//span[contains(text(),'Documents from Brookstone Capital Management')]"
    Then wait element "a > span:contains('REVIEW DOCUMENT')"
    And I click on "//a/span[contains(text(),'REVIEW DOCUMENT')]"
    Then I switch to new window
    And wait element "input[type=checkbox]"
    And I click on "//input[@type='checkbox']"
    Then I click on "//button[@track='continue-button']"
    And I switch to frame "signingIframe"
    And I click on "//div[@class='NavHelperNext']/button"
    And I click on "//input[contains(@src[1],'signing') and contains(@style, 'display: block')]" 
    And я переключаюсь на фрейм "frameUpload"
    And I wait element "input[id='fileSimple']"
    Then I attach the file "/home/roo/Загрузки/164023549_o.jpg" to "fileSimple"
    Then I switch to new window
    And I wait element "button[id='action-bar-bottom-btn-finish']"
    And I click on "//button[@id='action-bar-bottom-btn-finish']"
    And I wait element "div[class='save-a-copy dialog']"

    Then I switch to first window
    And I click on "//a[contains(@href, '#inbox')]"

    And I wait element "span:contains('Почта')"
    And I click on "//span[contains(text(),'Documents from Brookstone Capital Management')]"
    Then wait element "a > span:contains('REVIEW DOCUMENT')"
    And I click on "//a/span[contains(text(),'REVIEW DOCUMENT')]"
    Then I switch to new window
    And wait element "input[type=checkbox]"
    And I click on "//input[@type='checkbox']"
    Then I click on "//button[@track='continue-button']"
    And I switch to frame "signingIframe"

    Then I click on "//div[@class='NavHelperNext']/button"
    And I click on "//input[contains(@src,'signing') and contains(@style, 'display: block')]" 
    And I click on "//button[@value='initials']"

    Then I click on "//div[@class='NavHelperNext']/button"
    And I click on "//input[contains(@src,'signing') and contains(@style, 'display: block')]" 
    And I click on "//input[contains(@src,'signing') and contains(@style, 'display: block')]" 

    Then I switch to new window
    And I wait element "button[id='action-bar-bottom-btn-finish']"
    And I click on "//button[@id='action-bar-bottom-btn-finish']"
    And I wait element "div[class='save-a-copy dialog']"

    And I stop session "mail"

    #END SIGNARTURE

    Then I click on "//span[@class='icon action']"

    #And I click on "//div[contains(@id,'edit-tab')]//a[contains(text(),'Delete Entry')]"
    #Then I accept browser alert


