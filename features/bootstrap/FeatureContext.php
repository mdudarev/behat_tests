<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
//class FeatureContext implements Context, SnippetAcceptingContext
class FeatureContext extends MinkContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
 
    }
    
     /**
     * @Given /^(?:|я )кликаю на "([^"]*)"$/
     * @Given /^(?:|I )click on "([^"]*)"$/
     */
    public function iaKlikaiuNa($locator)
    {
        
        $page = $this->getSession()->getPage();
        
        $element = $page->find('xpath', $locator);
        
        if (!$element) {

            throw new Exception('element not found');
        }

        $element->click();
        sleep(1);

        
    }

    /**
     * @Given я ввожу каптчу в поле :locator
     */
    public function iaVvozhuKaptchuVPolie($locator)
    {   
        $page = $this->getSession()->getPage();
        $captcha = $page->find("xpath","//input[@name='captcha_sid']")->getValue();
        $code = file_get_contents('http://vds-f237.1gb.ru/local/backend/captcha_hack.php?code='.$captcha);

        if (!$code) {

            throw new Exception('Не удалось получить код каптчи');
        }

        $page->fillField('Число с картинки', $code);
    }

    /**
     * @Then я выбираю случайный элемент из списка :arg1
     */
    public function iaVybiraiuSluchainyiEliemientIzSpiska($locator)
    {
        $page = $this->getSession()->getPage();
        $element = $page->findAll('xpath', $locator);
        //var_dump(count($element));
        //var_dump($page->find('xpath', '//a[text()="Алтай Республика"]')->isVisible());
        //while (!($page->find('xpath', '//a[text()="Алтай Республика"]')->isVisible())) {
            
        //}
    }

    /**
     * @Given я раскрываю случайный элемент списка :arg1
     */
    public function iaRaskryvaiuSluchainyiEliemientSpiska($locator)
    {
        $page = $this->getSession()->getPage();
        $element = $page->findAll('xpath', $locator);
        $element[rand(0,count($element)-1)]->click();
        sleep(3);
        $element = $page->findAll('xpath', $locator.'/../ul/li/span');

        $element[rand(0,count($element)-1)]->click();
        sleep(3);
        $screenshot = $this->getSession()->getDriver()->getScreenshot();
        file_put_contents('screenshots/test'. date("m-d-y-H-i-s"). '.png', $screenshot);

    }

     /**
     * @Given /^(?:|я )не авторизован$/
     * @Given /^(?:|I )am not authorized$/
     */
    public function iaNieAvtorizovan()
    {
        $page = $this->getSession()->getPage();
        //Кнопка входа
        $openAuthBtn    = $page->find('css', 'button[id="_submit"]');
        //кнопка выхода
        $logoutBtn      = $page->find('css', 'a[id="logout"]');
        
        if ($logoutBtn && !$openAuthBtn) {
            throw new Exception('Пользователь уже авторизован');
        }
    }

    /**
     * @Given /^(?:|я )авторизуюсь под пользователем "([^"]*)" с паролем "([^"]*)"$/
     * @Given /^(?:|I )log in as a user "([^"]*)" with a password "([^"]*)"$/
     */
    public function iaAvtorizuiusPodPolZovatieliem($name, $pass)
    {
        $page = $this->getSession()->getPage();

        $openAuthBtn = $page->find('css', 'button[id="_submit"]');
        
        //если ненайден
        if (!$openAuthBtn) {

            throw new Exception('Кнопка входа не найдена');
        }

        $openAuthBtn->click();

        $page->fillField('username', $name);
        $page->fillField('password', $pass);

        $page->pressButton('Login');

        $errorText = $page->find('css', 'div[class="error-msg"]');

        if ($errorText) {

            throw new Exception('Неверный логин или пароль');        
        } 
    }

     /**
     * @Given /^(?:|я )авторизован$/
     * @Given /^(?:|я )должен быть авторизован$/
     * @Given /^(?:|I )should be authorized$/
     */
    public function iaAvtorizovan()
    {
        $page = $this->getSession()->getPage();
        //Кнопка входа
        $openAuthBtn    = $page->find('css', 'button[id="_submit"]');
        //кнопка выхода
        $logoutBtn      = $page->find('css', 'a[id="logout"]');
        
        if ($openAuthBtn && !$logoutBtn) {
            throw new Exception('Пользователь не авторизован');
        }
    }

    /**
     * @Given /^(?:|я )создаю проспект с полями:$/
     * @Given /^(?:|I )create prospect with fields:$/
     */
    public function iaSozdaiuProspiektSPoliami(TableNode $table)
    {
        $page = $this->getSession()->getPage();
        $hash = $table->getHash();
        foreach ($hash as $row) {
            $page->fillField('vanare_websitebundle_prospect_firstName', $row['First Name']);
            $page->fillField('vanare_websitebundle_prospect_lastName', $row['Last Name']);
            $page->fillField('vanare_websitebundle_prospect_email', $row['Work Email']);
            $this->getSession()->getPage()->selectFieldOption('vanare_websitebundle_prospect_repCode','7E5');
            $this->getSession()->evaluateScript("$('button').click();");
            //$page->pressButton('Continue');
        }
    }

    /**
     * @Then /^(?:|я )нажимаю на "([^"]*)" через js$/
     * @Then /^(?:|I )press on "([^"]*)" by js$/
     */
    public function iaNazhimaiuNaChieriezfJs($locator)
    {
        $this->getSession()->evaluateScript("$('{$locator}').click();");
        sleep(1);
    }

    /**
     * @Given /^(?:|я )не вижу ошибки$/
     * @Given /^(?:|я )не должен видеть ошибки$/
     * @Given /^(?:|I )do not see errors$/
     */
    public function iaNieVizhuOshibki()
    {
        $page = $this->getSession()->getPage();

        $errorMsg = $page->find('xpath', "//input[contains(@class,'error')]");

        if ($errorMsg) {
            $sleep(50);
            throw new Exception('Ошибки: ' . $errorMsg);
        }     
    }

    /**
     * @Then /^(?:|я )заполняю "([^"]*)" в поле "([^"]*)"$/
     * @Then /^(?:|I )enter "([^"]*)" in field "([^"]*)"$/
     */
    public function iaZapolniaiuVPolie($value, $xpath)
    {   
        $this->getSession()->getDriver()->focus($xpath);
        $this->getSession()->getDriver()->setValue($xpath, $value);
        //sleep(5);
    }

    /**
     * @Given /^(?:|я )жду элемент "([^"]*)"$/
     * @Given /^(?:|I )wait element "([^"]*)"$/
     */
    public function iaZhduEliemient($locator)
    {

        $this->waitForElementPresent($locator);
    
    }


    public function waitForElementPresent($locator, $timeout = 60)
    {
        for ($second = 0; ; $second++) {
            
            if ($second >= $timeout) { 
                
                throw new Exception('element not found'); 
                break;  
            }
            
            if($this->getSession()->getPage()->find('css', $locator)) { 
                break; 
            }   

            sleep(1);
        }
    }

    /**
     * @Then /^(?:|я )подтверждаю действие$/
     * @Then /^(?:|I )accept browser alert$/
     */
    public function iaPodtvierzhdaiuDieistviie()
    {
        $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();
    }

    /**
     * @Then /^(?:|я )ввожу "([^"]*)" в поле "([^"]*)" через js$/
     */
    public function iaVvozhuVPolieChieriezJs($locator, $value)
    {
        $this->getSession()->evaluateScript("$('{$locator}').value='{$value}';");
        //sleep(10);
    }

    /**
     * @Then /^(?:|я )создаю новую сессию "([^"]*)"$/
     */
    public function iaSozdaiuNovuiuSiessiiu($sessionName)
    {
        //echo $this->getMink()->getDefaultSessionName();
        $driver = new \Behat\Mink\Driver\Selenium2Driver();

        $this->getMink()->registerSession($sessionName, new \Behat\Mink\Session($driver));
        $this->getMink()->setDefaultSessionName($sessionName);
        //$this->getSession()->start();
        //echo $this->getMink()->getDefaultSessionName();


    }

    /**
     * @Then /^(?:|я )останавливаю сессию "([^"]*)"$/
     * @Then /^(?:|I )stop session "([^"]*)"$/
     */
    public function iaOstanavlivaiuSiessiiu($sessionName)
    {
        $session = $this->getSession($sessionName);
        $session->stop();
        sleep(10);
        $this->getMink()->setDefaultSessionName('selenium2');
    }

    /**
     * @Then /^(?:|я )переключаюсь на новое окно$/
     * @Then /^(?:|I )switch to new window$/
     */
    public function iaPieriekliuchaiusNaNovoieOkno()
    {
        $windows = $this->getSession()->getDriver()->getWindowNames();
        //var_dump($this->getSession()->getDriver()->getWindowName());
        $nmb = (count($this->getSession()->getDriver()->getWindowNames()))-1;
        $this->getSession()->getDriver()->switchToWindow($windows[$nmb]);
    }

    /**
     * @Then /^(?:|я )переключаюсь на фрейм "([^"]*)"$/
     * @Then /^(?:|I )switch to frame "([^"]*)"$/
     */
    public function iaPieriekliuchaiusNaFrieim($id)
    {
        $this->getSession()->getDriver()->switchToIFrame($id);
    }

    /**
     * @Then /^(?:|I )focus on element "([^"]*)"$/
     */
    public function iFocusOnElement($xpath)
    {
        $this->getSession()->getDriver()->focus($xpath);
        //sleep(10);
    }

    /**
     * @Then /^(?:|я )переключаюсь на первое окно$/
     * @Then /^(?:|I )switch to first window$/
     */
    public function switchToFirstWindow()
    {
        $windows = $this->getSession()->getDriver()->getWindowNames();
        //var_dump($this->getSession()->getDriver()->getWindowName());
        //$nmb = (count($this->getSession()->getDriver()->getWindowNames()))-1;
        $this->getSession()->getDriver()->switchToWindow($windows[0]);
    }





}
