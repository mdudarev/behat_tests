# language: en

Feature: Grow Practice - Account
  Testing account creation and signature.
 
  @javascript
  Scenario: Autorize, create prospect, check statuses, create trust account and sent documents, check statuses after signature and delete prospect.
    Given I am on homepage
    And I am not authorized
    And I log in as a user "Mikhail" with a password "Pa$$word_1"
    Then I should be authorized
    Then I should see "Welcome to Vanare"
    And I follow "Grow Practice"
    And I follow "+ Add New Prospect"
    Then I create prospect with fields:
    |First Name|Last Name|Advisor Code|Work Email|
    |Mikahil|Dudarev|TEST|md+1@4xxi.com|
    Then I click on "//span[@class='icon action']"
    And I click on "//div[contains(@id,'edit-tab')]//a[contains(text(),'Operational Status')]"
    #Then I should see элемент "//div[@id='modal-first-step' and @class='status-block-step -active']"
    #And I should see элемент "//div[@id='modal-sixth-step' and @class='status-block-step -active']"
    Then I click on "//div[@class='modal-dialog']//button[@class='close']"
    And I click on "//span[text()='+ Account'][ancestor::tr/td[text()='Dudarev']]"
    And I click on "//label/p[text()='Personal Trust']"
    And I focus on element "//select[@id='vanare_websitebundle_newaccount_workflow_step1_trustNumberOfTrustees']"
    And I click on "//select[@id='vanare_websitebundle_newaccount_workflow_step1_trustNumberOfTrustees']"
    And I select "1" from "vanare_websitebundle_newaccount_workflow_step1_trustNumberOfTrustees"
    And I press on "button" by js

    #trust info

    And I should see "Basic"

    Then I enter "Auto Test Trust" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2trust_title']"
    Then I enter "123456789" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2trust_taxIdNumber']"

    Then I fill in "03/27/2015" for "vanare_websitebundle_newaccount_workflow_step2trust_dateFoundation"
    Then I fill in "03/27/2015" for "vanare_websitebundle_newaccount_workflow_step2trust_dateEffective"
    Then I fill in "03/27/2015" for "vanare_websitebundle_newaccount_workflow_step2trust_dateOfLastAmendment"

    And I press on "button" by js
    And I do not see errors

    # Trustee information

    Then I fill in "777" for "vanare_websitebundle_newaccount_workflow_step2trustee_phone1"
    Then I fill in "777" for "vanare_websitebundle_newaccount_workflow_step2trustee_phone2"
    Then I fill in "7777" for "vanare_websitebundle_newaccount_workflow_step2trustee_phone3"

    Then I fill in "Test Street" for "vanare_websitebundle_newaccount_workflow_step2trustee_homeAddress1"
    Then I fill in "Orenburg" for "vanare_websitebundle_newaccount_workflow_step2trustee_homeCity"
    Then I fill in "46545" for "vanare_websitebundle_newaccount_workflow_step2trustee_homeZip"
    
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_USCitizenship_0']"
    And I press on "label[for=vanare_websitebundle_newaccount_workflow_step2_ssn_1]" by js
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_ssn_1']"
   

    Then I enter "11" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2trustee_dateOfBirth_month']"
    Then I enter "12" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2trustee_dateOfBirth_day']"
    Then I enter "1988" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2trustee_dateOfBirth_year']"

    And I select "Single" from "vanare_websitebundle_newaccount_workflow_step2trustee_maritialStatus"
    Then I enter "2" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2trustee_dependents']"
    
    Then I fill in "12354564555" for "vanare_websitebundle_newaccount_workflow_step2trustee_driverLicense"
    Then I fill in "03/27/2015" for "vanare_websitebundle_newaccount_workflow_step2trustee_expiration"

    
    And I press on "input[id=vanare_websitebundle_newaccount_workflow_step2trustee_employment]" by js
    And I select "Unemployed" from "vanare_websitebundle_newaccount_workflow_step2trustee_employment"
    Then I fill in "Test this" for "vanare_websitebundle_newaccount_workflow_step2trustee_sourceOfIncome"
    
    
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_afiliationsFinancial_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_afiliationsFinancial_1']"
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_afiliationsDirector_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_afiliationsDirector_1']"
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_afiliationsPolitical_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2trustee_afiliationsPolitical_1']"

    And I press on "button" by js

    And I do not see errors
    
    And I should see "Cash Sweep Vehicle"
    And I press on "button" by js

    And I should see "Proxy Authorization"
    And I press on "button" by js

    And I should see "Duplicate Accounts"
    And I press on "button" by js

    And I should see "Margin"
    And I press on "button" by js

    And I should see "Personal Bank Account Information"
    And I press on "button" by js

    And I should see "Transfer Assets into Your Account"
    And I press on "button" by js
    
    And I should see "E-Signature"
    And I press "vanare_websitebundle_newaccount_workflow_step14_submit"

    And I wait element "p:contains('New Account Documents sent to Mikahil Dudarev')"


    Then follow "Grow Practice"
    Then I click on "//span[@class='icon action']"

    And I click on "//div[contains(@id,'edit-tab')]//a[contains(text(),'Delete Entry')]"
    Then I accept browser alert


