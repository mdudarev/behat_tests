# language: ru

Функционал: Grow Practice - Account
  Проверяем всё что связано с аккаунтом
 
  @javascript
  Сценарий: Авторизуемся, создаём проспект, проверяем статус, создаём аккаунт и отправляем документы на подписание, проверяем статусы и удаляем проспект.
    Допустим я на главной странице
    #Тогда код ответа сервера должен быть 200
    И я не авторизован
    И я авторизуюсь под пользователем "Mikhail" с паролем "Pa$$word_1"
    Тогда я должен быть авторизован
    И я должен видеть "Welcome to Vanare"
    И я кликаю по ссылке "Grow Practice"
    #Тогда код ответа сервера должен быть 200
    #И я должен видеть "+ Add New Prospect"
    И я кликаю по ссылке "+ Add New Prospect"
    #И я должен видеть "New Prospect"
    Тогда я создаю проспект с полями:
    |First Name|Last Name|Advisor Code|Work Email|
    |Mikahil|Dudarev|TEST|md+1@4xxi.com|
    #И должен видеть текст соответствующий "Dudarev"
    Тогда я кликаю на "//span[@class='icon action']"
    И я кликаю на "//div[contains(@id,'edit-tab')]//a[contains(text(),'Operational Status')]"
    #Тогда я должен видеть элемент "//div[@id='modal-first-step' and @class='status-block-step -active']"
    #И я должен видеть элемент "//div[@id='modal-sixth-step' and @class='status-block-step -active']"
    Тогда я кликаю на "//div[@class='modal-dialog']//button[@class='close']"
    И я кликаю на "//span[text()='+ Account'][ancestor::tr/td[text()='Dudarev']]"
    И я кликаю на "//label/p[text()='Individual']"
    И я нажимаю на "button" через js
    Тогда я ввожу "777" в поле "vanare_websitebundle_newaccount_workflow_step2_phone1"
    Тогда я ввожу "777" в поле "vanare_websitebundle_newaccount_workflow_step2_phone2"
    Тогда я ввожу "7777" в поле "vanare_websitebundle_newaccount_workflow_step2_phone3"
    Тогда я ввожу "Test Street" в поле "vanare_websitebundle_newaccount_workflow_step2_homeAddress1"
    Тогда я ввожу "Orenburg" в поле "vanare_websitebundle_newaccount_workflow_step2_homeCity"
    Тогда я ввожу "46545" в поле "vanare_websitebundle_newaccount_workflow_step2_homeZip"
    
    И я кликаю на "//label[@for='vanare_websitebundle_newaccount_workflow_step2_USCitizenship_0']"
    И я нажимаю на "label[for=vanare_websitebundle_newaccount_workflow_step2_ssn_1]" через js
    И я кликаю на "//label[@for='vanare_websitebundle_newaccount_workflow_step2_ssn_1']"
   

    Тогда я заполняю "11" в поле "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_month']"

    Тогда я заполняю "12" в поле "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_day']"

    Тогда я заполняю "1988" в поле "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_year']"
    
    

    И я выбираю "Single" в поле "vanare_websitebundle_newaccount_workflow_step2_maritialStatus"
    Тогда я ввожу "2" в поле "vanare_websitebundle_newaccount_workflow_step2_dependents"
    
    Тогда я ввожу "Test this" в поле "vanare_websitebundle_newaccount_workflow_step2_driverLicense"
    Тогда я ввожу "03/27/2015" в поле "vanare_websitebundle_newaccount_workflow_step2_expiration"

    
    И я нажимаю на "input[id=vanare_websitebundle_newaccount_workflow_step2_employment]" через js
    И я выбираю "Unemployed" в поле "vanare_websitebundle_newaccount_workflow_step2_employment"
    Тогда я ввожу "Test this" в поле "vanare_websitebundle_newaccount_workflow_step2_sourceOfIncome"
    И я нажимаю на "input[id=vanare_websitebundle_newaccount_workflow_step2_afiliationsDirector_1]" через js
    
    
    И я кликаю на "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsFinancial_1']"
    И я кликаю на "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsDirector_1']"
    И я кликаю на "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsPolitical_1']"
    И я нажимаю на "button" через js
    И я не вижу ошибки

    И я должен видеть "Beneficiary Information"
    И я нажимаю на "button" через js
    
    И я должен видеть "Cash Sweep Vehicle"
    И я нажимаю на "button" через js

    И я должен видеть "Proxy Authorization"
    И я нажимаю на "button" через js

    И я должен видеть "Duplicate Accounts"
    И я нажимаю на "button" через js

    И я должен видеть "Margin"
    И я нажимаю на "button" через js

    И я должен видеть "Personal Bank Account Information"
    И я нажимаю на "button" через js

    И я должен видеть "Transfer Assets into Your Account"
    И я нажимаю на "button" через js
    
    И я должен видеть "E-Signature"
    И я нажимаю "vanare_websitebundle_newaccount_workflow_step14_submit"

    И я жду элемент "p:contains('New Account Documents sent to Mikahil Dudarev')"


    Тогда я кликаю по ссылке "Grow Practice"
    Тогда я кликаю на "//span[@class='icon action']"

    И я кликаю на "//div[contains(@id,'edit-tab')]//a[contains(text(),'Delete Entry')]"
    Тогда я подтверждаю действие


