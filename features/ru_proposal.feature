# language: ru

Функционал: Grow Practice - Proposal
  Проверяем всё что связано с Proposal
 
  @javascript
  Сценарий: Открыть форму авторизации
    Допустим я на главной странице
    #Тогда код ответа сервера должен быть 200
    И я не авторизован
    И я авторизуюсь под пользователем "Mikhail" с паролем "Pa$$word_1"
    Тогда я должен быть авторизован
    И я должен видеть "Welcome to Vanare"
    И я кликаю по ссылке "Grow Practice"
    #Тогда код ответа сервера должен быть 200
    #И я должен видеть "+ Add New Prospect"
    И я кликаю по ссылке "+ Add New Prospect"
    #И я должен видеть "New Prospect"
    Тогда я создаю проспект с полями:
    |First Name|Last Name|Advisor Code|Work Email|
    |Mikahil|Dudarev|TEST|russia.hummer@gmail.com|
    #И должен видеть текст соответствующий "Dudarev"
    Тогда я кликаю на "//span[@class='icon action']"
    И я кликаю на "//div[contains(@id,'edit-tab')]//a[contains(text(),'Operational Status')]"
    #Тогда я должен видеть элемент "//div[@id='modal-first-step' and @class='status-block-step -active']"
    #И я должен видеть элемент "//div[@id='modal-sixth-step' and @class='status-block-step -active']"
    Тогда я кликаю на "//div[@class='modal-dialog']//button[@class='close']"
    И я кликаю на "//span[text()='+ Proposal'][ancestor::tr/td[text()='Dudarev']]"
    И я кликаю по ссылке "TEST"
    И я должен видеть "TEST"

    И я кликаю на "//label[text()='100-200K']"
    Тогда я нажимаю "Next"

    И я жду элемент "label:contains('Keep pace with inflation')"
    И я кликаю на "//label[text()='Keep pace with inflation']"
    Тогда я нажимаю "Next"

    И я нажимаю на "input" через js
    #И я жду элемент "input"
    И я ввожу "150000" в поле "input" через js
    Тогда я заполняю "1500000" в поле "//input[@class='questionnaire-textinput i-fill-line price-field']"
    Тогда я нажимаю "Finish"
    И я жду элемент "p:contains('Confirm my answers and go to Strategy Selection')"
    Тогда я кликаю на "//a[@id='btn-proposal-next']"

    И я жду элемент "h2:contains('Model Portfolio')"
    Тогда я должен видеть "Model Portfolio"

    Тогда я заполняю "60" в поле "//input[@id='proposal_step2_model_components_0_assetClass_instruments_1_ratio']"
    Тогда я заполняю "25" в поле "//input[@id='proposal_step2_model_components_0_assetClass_instruments_2_ratio']"

    Тогда я заполняю "25" в поле "//input[@id='proposal_step2_model_components_1_assetClass_instruments_1_ratio']"
    Тогда я заполняю "15" в поле "//input[@id='proposal_step2_model_components_1_assetClass_instruments_2_ratio']"

    Тогда я нажимаю на "button[id=save-form-btn]" через js

    И я жду элемент "iframe[class='hidden-levers-frame']"
    И я кликаю на "//a[contains(text(),'Next')]"

    Тогда я должен видеть "Account Information"


    Тогда я кликаю по ссылке "Grow Practice"
    Тогда я кликаю на "//span[@class='icon action']"

    И я кликаю на "//div[contains(@id,'edit-tab')]//a[contains(text(),'Delete Entry')]"
    Тогда я подтверждаю действие


		
		    