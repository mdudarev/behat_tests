# language: en

Feature: Grow Practice - Account
  Testing account creation and signature.
 
  @javascript
  Scenario: Autorize, create prospect, check statuses, create trust account and sent documents, check statuses after signature and delete prospect.
    Given I am on homepage
    And I am not authorized
    And I log in as a user "Mikhail" with a password "Pa$$word_1"
    Then I should be authorized
    Then I should see "Welcome to Vanare"
    And I follow "Grow Practice"
    And I follow "+ Add New Prospect"
    Then I create prospect with fields:
    |First Name|Last Name|Advisor Code|Work Email|
    |Mikahil|Dudarev|TEST|md+1@4xxi.com|
    Then I click on "//span[@class='icon action']"
    And I click on "//div[contains(@id,'edit-tab')]//a[contains(text(),'Operational Status')]"
    #Then I should see элемент "//div[@id='modal-first-step' and @class='status-block-step -active']"
    #And I should see элемент "//div[@id='modal-sixth-step' and @class='status-block-step -active']"
    Then I click on "//div[@class='modal-dialog']//button[@class='close']"
    And I click on "//span[text()='+ Account'][ancestor::tr/td[text()='Dudarev']]"
    And I click on "//label/p[text()='IRA']"
    And I press on "button" by js
    Then I fill in "777" for "vanare_websitebundle_newaccount_workflow_step2_phone1"
    Then I fill in "777" for "vanare_websitebundle_newaccount_workflow_step2_phone2"
    Then I fill in "7777" for "vanare_websitebundle_newaccount_workflow_step2_phone3"
    Then I fill in "Test Street" for "vanare_websitebundle_newaccount_workflow_step2_homeAddress1"
    Then I fill in "Orenburg" for "vanare_websitebundle_newaccount_workflow_step2_homeCity"
    Then I fill in "46545" for "vanare_websitebundle_newaccount_workflow_step2_homeZip"
    
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_USCitizenship_0']"
    And I press on "label[for=vanare_websitebundle_newaccount_workflow_step2_ssn_1]" by js
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_ssn_1']"
   

    Then I enter "11" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_month']"

    Then I enter "12" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_day']"

    Then I enter "1988" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dateOfBirth_year']"
    
    

    And I select "Single" from "vanare_websitebundle_newaccount_workflow_step2_maritialStatus"
    Then I enter "2" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step2_dependents']"
    
    Then I fill in "Test this" for "vanare_websitebundle_newaccount_workflow_step2_driverLicense"
    Then I fill in "03/27/2015" for "vanare_websitebundle_newaccount_workflow_step2_expiration"

    
    And I focus on element "//select[@id='vanare_websitebundle_newaccount_workflow_step2_employment']"
    And I select "Unemployed" from "vanare_websitebundle_newaccount_workflow_step2_employment"
    And I focus on element "//input[@id='vanare_websitebundle_newaccount_workflow_step2_sourceOfIncome']"
    Then I fill in "Test this" for "vanare_websitebundle_newaccount_workflow_step2_sourceOfIncome"   
    
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsFinancial_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsFinancial_1']"
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsDirector_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsDirector_1']"
    And I focus on element "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsPolitical_1']"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step2_afiliationsPolitical_1']"
    
    And I press on "button" by js
    And я не вижу ошибки

    #"Beneficiary Information"

    And I should see "Beneficiary Information"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step6_hasBeneficiaries_1']"
    And I wait element "span:contains('Beneficiary #1')"

    Then I fill in "Petr" for "vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_firstName"
    Then I fill in "Petrov" for "vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_lastName"
    Then I fill in "Testing" for "vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_relationship"
    Then I fill in "666" for "vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_ssn1"
    Then I fill in "77" for "vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_ssn2"
    Then I fill in "8888" for "vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_ssn3"

    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_typeOfBeneficiary_0']"

    Then I enter "11" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_dateOfBirth_month']"
    Then I enter "12" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_dateOfBirth_day']"
    Then I enter "1988" in field "//input[@id='vanare_websitebundle_newaccount_workflow_step6_beneficiary_0_dateOfBirth_year']"
    
    And I press on "button" by js
    
    And I should see "Cash Sweep Vehicle"
    And I press on "button" by js

    And I should see "Proxy Authorization"
    And I press on "button" by js

    And I should see "Duplicate Accounts"
    And I press on "button" by js

    And I should see "Margin"
    And I press on "button" by js

    #Personal Bank Account Information

    Then I should see "Personal Bank Account Information"
    And I click on "//label[@for='vanare_websitebundle_newaccount_workflow_step12_fundsTransfer_1']"
    And I wait element "h2:contains('Incoming & Outgoing Funds Transfer')"

    Then I fill in "Nikita" for "vanare_websitebundle_newaccount_workflow_step12_ownerFirstName"
    Then I fill in "Dedov" for "vanare_websitebundle_newaccount_workflow_step12_ownerLastName"
    Then I fill in "Testov" for "vanare_websitebundle_newaccount_workflow_step12_bankName"
    Then I fill in "12378945123" for "vanare_websitebundle_newaccount_workflow_step12_bankAccount"
    Then I fill in "789123456" for "vanare_websitebundle_newaccount_workflow_step12_transitRouting"
    Then I fill in "Account title" for "vanare_websitebundle_newaccount_workflow_step12_accountTitle"

    And I click on "//input[@id='vanare_websitebundle_newaccount_workflow_step12_typeOfAccount_0']"

    And I press on "button" by js

    And I should see "Transfer Assets into Your Account"
    And I press on "button" by js
    
    And I should see "E-Signature"
    And I press "vanare_websitebundle_newaccount_workflow_step14_submit"

    And I wait element "p:contains('New Account Documents sent to Mikahil Dudarev')"


    Then follow "Grow Practice"
    Then I click on "//span[@class='icon action']"

    #And I click on "//div[contains(@id,'edit-tab')]//a[contains(text(),'Delete Entry')]"
    #Then I accept browser alert


