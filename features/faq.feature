# language: en

Feature: Grow Practice - Proposal
  Проверяем всё что связано с Proposal
 
  @javascript
  Scenario: Test
    Given I am on homepage
    And I am not authorized
    #And I log in as a user "Mikhail" with a password "Pa$$word_1"
    #Then I should be authorized
    #Then I should see "Welcome to Vanare"
    #And I follow "Grow Practice"
    #And I focus on element "//td[contains(text(), '03/12/2015')]"
    Then я создаю новую сессию "mail"
    Then go to "http://gmail.com"
    And I fill in "md@4xxi.com" for "Email"
    And I fill in "$58" for "Passwd"
    Then I press "signIn"
    And I wait element "span:contains('Почта')"
    And I click on "//span[contains(text(),'Documents from Brookstone Capital Management')]"
    Then wait element "a > span:contains('REVIEW DOCUMENT')"
    And I click on "//a/span[contains(text(),'REVIEW DOCUMENT')]"
    Then I switch to new window
    And wait element "input[type=checkbox]"
    And I click on "//input[@type='checkbox']"
    Then I click on "//button[@track='continue-button']"
    And I switch to frame "signingIframe"
    And I click on "//div[@class='NavHelperNext']/button"
    And I click on "//input[contains(@src[1],'signing') and contains(@style, 'display: block')]" 
    And я переключаюсь на фрейм "frameUpload"
    And I wait element "input[id='fileSimple']"
    Then I attach the file "/home/roo/Загрузки/164023549_o.jpg" to "fileSimple"
    Then I switch to new window
    And I wait element "button[id='action-bar-bottom-btn-finish']"
    And I click on "//button[@id='action-bar-bottom-btn-finish']"
    And I wait element "div[class='save-a-copy dialog']"

    Then I switch to first window
    And I click on "//a[contains(@href, '#inbox')]"

    And I wait element "span:contains('Почта')"
    And I click on "//span[contains(text(),'Documents from Brookstone Capital Management')]"
    Then wait element "a > span:contains('REVIEW DOCUMENT')"
    And I click on "//a/span[contains(text(),'REVIEW DOCUMENT')]"
    Then I switch to new window
    And wait element "input[type=checkbox]"
    And I click on "//input[@type='checkbox']"
    Then I click on "//button[@track='continue-button']"
    And I switch to frame "signingIframe"

    Then I click on "//div[@class='NavHelperNext']/button"
    And I click on "//input[contains(@src,'signing') and contains(@style, 'display: block')]" 
    And I click on "//button[@value='initials']"

    Then I click on "//div[@class='NavHelperNext']/button"
    And I click on "//input[contains(@src,'signing') and contains(@style, 'display: block')]" 
    And I click on "//input[contains(@src,'signing') and contains(@style, 'display: block')]" 

    Then I switch to new window
    And I wait element "button[id='action-bar-bottom-btn-finish']"
    And I click on "//button[@id='action-bar-bottom-btn-finish']"
    And I wait element "div[class='save-a-copy dialog']"

    And I stop session "mail"

    Then I am on homepage
